<?php

namespace Drupal\rest_view_mode\Form;

use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rest_view_mode\ExtraFieldManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Rest View Mode configuration form.
 */
class RestViewModeSettingsForm extends ConfigFormBase {

  use UseCacheBackendTrait;

  /**
   * The settings name.
   *
   * @var string
   */
  const SETTINGS = 'rest_view_mode.settings';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * The extra field manager.
   *
   * @var \Drupal\rest_view_mode\ExtraFieldManagerInterface
   */
  protected ExtraFieldManagerInterface $extraFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);

    /** @var EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->setEntityTypeManager($entity_type_manager);

    /** @var EntityTypeBundleInfoInterface $entity_type_bundle_info */
    $entity_type_bundle_info = $container->get('entity_type.bundle.info');
    $instance->setEntityTypeBundleInfo($entity_type_bundle_info);

    /** @var EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = $container->get('entity_field.manager');
    $instance->setEntityFieldManager($entity_field_manager);

    /** @var EntityDisplayRepositoryInterface $entityDisplayRepository */
    $entity_display_repository = $container->get('entity_display.repository');
    $instance->setEntityDisplayRepository($entity_display_repository);

    /** @var ExtraFieldManagerInterface $extra_field_manager */
    $extra_field_manager = $container->get('rest_view_mode.extra_field_manager');
    $instance->setExtraFieldManager($extra_field_manager);

    return $instance;
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the entity type bundle info.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function setEntityTypeBundleInfo(EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * Sets the entity field manager.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function setEntityFieldManager(EntityFieldManagerInterface $entity_field_manager): void {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Sets the entity display repository.
   *
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function setEntityDisplayRepository(EntityDisplayRepositoryInterface $entity_display_repository): void {
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * Sets the extra field manager.
   *
   * @param \Drupal\rest_view_mode\ExtraFieldManagerInterface $extra_field_manager
   *   The extra field manager.
   */
  public function setExtraFieldManager(ExtraFieldManagerInterface $extra_field_manager) {
    $this->extraFieldManager = $extra_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'rest_view_mode_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildForm(array $form, FormStateInterface $form_state, $resource_id = NULL): array {
    $config = $this->config(static::SETTINGS);
    $form['#title'] = $this->t('Rest View Mode settings');
    $form['#tree'] = TRUE;

    $entity_types = $this->entityTypeManager->getDefinitions();
    usort($entity_types, function ($a, $b) {
      return strcmp($a->getLabel(), $b->getLabel());
    });

    $extra_fields = $this->extraFieldManager->getAllDisplayExtraFields();

    $form['entities_tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    foreach ($entity_types as $entity_type) {
      if (
        $entity_type->hasViewBuilderClass() &&
        $entity_type->entityClassImplements(FieldableEntityInterface::class)
      ) {

        $bundles = $extra_fields[$entity_type->id()] ?? null;
        if ($bundles) {
          $bundle_list = $this->entityTypeBundleInfo->getBundleInfo($entity_type->id());

          $form['entities'][$entity_type->id()] = [
            '#type' => 'details',
            '#title' => $entity_type->getLabel(),
            '#group' => 'entities_tabs',
          ];

          $form['entities'][$entity_type->id()]['activated'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Expose %entity_type entity type in Rest View Mode resources', ['%entity_type' => $entity_type->id()]),
            '#default_value' => $config->get('entities.' . $entity_type->id() . '.activated') ?? 0,
            '#attributes' => [
              'data-activated' => 'entities_' . $entity_type->id(),
            ],
          ];

          $form['entities'][$entity_type->id()]['view_modes'] = [
            '#type' => 'checkboxes',
            '#title' => $this->t('View modes to expose in Rest View Mode resources'),
            '#description' => $this->t('If none are checked, all view modes will be exposed.'),
            '#options' => $this->entityDisplayRepository->getViewModeOptions($entity_type->id()),
            '#default_value' => $config->get('entities.' . $entity_type->id() . '.view_modes') ?? [],
            '#states' => [
              'enabled' => [
                ':input[name="entities[' . $entity_type->id() . '][activated]"]' => ['checked' => TRUE],
              ],
            ],
          ];

          $form['entities'][$entity_type->id()]['extra_fields'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Normalized %entity_type entity type properties to activate as extra fields, per bundle', ['%entity_type' => $entity_type->id()]),
            '#states' => [
              'enabled' => [
                ':input[name="entities[' . $entity_type->id() . '][activated]"]' => ['checked' => TRUE],
              ],
            ],
          ];

          foreach ($bundles as $bundle => $properties) {
            $form['entities'][$entity_type->id()]['extra_fields'][$bundle] = [
              '#type' => 'details',
              '#title' => $bundle_list[$bundle]['label'] ?? $bundle,
              '#description' => $this->t('Select the display extra fields based on entity properties that you wish to activate. Activated extra fields will be available in the entity view modes.'),
            ];
            $form['entities'][$entity_type->id()]['extra_fields'][$bundle]['properties'] = [
              '#type' => 'checkboxes',
              '#options' => array_combine(
                array_keys($properties['display']),
                array_column($properties['display'], 'label')
              ),
              '#default_value' => $config->get('entities.' . $entity_type->id() . '.extra_fields.' . $bundle) ?? [],
            ];
          }
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_values = [];
    foreach ($form_state->getValue('entities') as $entity_type_id => $entity_type_config) {

      if ($entity_type_config['activated']) {

        $config_values[$entity_type_id]['activated'] = TRUE;

        foreach ($entity_type_config['view_modes'] as $view_mode => $view_mode_status) {
          if ($view_mode_status) {
            $config_values[$entity_type_id]['view_modes'][] = $view_mode;
          }
        }

        foreach ($entity_type_config['extra_fields'] as $bundle => $config) {
          foreach ($config['properties'] as $property => $activated) {
            if ($activated) {
              $config_values[$entity_type_id]['extra_fields'][$bundle][] = $property;
            }
          }
        }
      }
    }
    $this->config(static::SETTINGS)
      ->set('entities', $config_values)
      ->save();

    $this->entityFieldManager->clearCachedFieldDefinitions();

    parent::submitForm($form, $form_state);
  }

}
