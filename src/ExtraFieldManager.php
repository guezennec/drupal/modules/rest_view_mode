<?php

namespace Drupal\rest_view_mode;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class ExtraFieldManager implements ExtraFieldManagerInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Constructs a new ExtraFieldManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllDisplayExtraFields(): array {
    $extra_fields = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if (
        $entity_type->hasViewBuilderClass() &&
        $entity_type->entityClassImplements(FieldableEntityInterface::class)
      ) {
        foreach (array_keys($this->entityTypeBundleInfo->getBundleInfo($entity_type->id())) as $bundle) {
          foreach ($this->entityFieldManager->getBaseFieldDefinitions($entity_type->id()) as $base_field_id => $base_field) {
            $extra_fields[$entity_type->id()][$bundle]['display']['rest_view_mode_' . $base_field_id] = [
              'label' => $this->t('@entity normalized %property', [
                '@entity' => $entity_type->getLabel(),
                '%property' => $base_field['label'],
              ]),
              'description' => $this->t('Normalized extra field for %property entity property.', ['%property' => $base_field_id]),
              'visible' => FALSE,
            ];
          }
        }
      }
    }

    return $extra_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayExtraFieldInfo(): array {
    $all_display_extra_fields = $this->getAllDisplayExtraFields();
    $activated_extra_fields = [];

    $config = $this->configFactory->get('rest_view_mode.settings');
    $available_properties = $config->get('entities');

    foreach ($all_display_extra_fields as $entity_type_id => $bundles) {
      foreach ($bundles as $bundle => $properties) {
        foreach ($properties['display'] as $property => $property_info) {
          if (
            !empty($available_properties[$entity_type_id]['extra_fields'][$bundle]) &&
            in_array($property, $available_properties[$entity_type_id]['extra_fields'][$bundle])
          ) {
            $activated_extra_fields[$entity_type_id][$bundle]['display'][$property] = $property_info;
          }
        }
      }
    }

    return $activated_extra_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function entityView(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    foreach ($display->getComponents() as $name => $options) {
      if ($property = $this->getProperty($name, $entity)) {
        $build[$name] = $property->view($view_mode);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProperty(string $name, EntityInterface $entity): ?FieldItemListInterface {
    if (!str_starts_with($name, 'rest_view_mode_')) {
      return NULL;
    }
    $property_name = substr($name, strlen('rest_view_mode_') - (strlen($name)));
    $property = $entity->get($property_name);
    if (!$property->access('view')) {
      return NULL;
    }

    return $property;
  }

}
