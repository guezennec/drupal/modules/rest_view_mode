<?php

namespace Drupal\rest_view_mode;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;

interface ExtraFieldManagerInterface {

  /**
   * Gets extra field info from all entity properties.
   *
   * @return array
   *   The extra field info array for entities.
   * @see \hook_entity_extra_field_info()
   */
  public function getAllDisplayExtraFields(): array;

  /**
   * Gets all the display extra fields from entity properties.
   *
   * @return array
   *   The extra fields.
   * @see \rest_view_mode_entity_extra_field_info().
   */
  public function getDisplayExtraFieldInfo(): array;

  /**
   * Enrich entity build with extra fields on view.
   *
   * @param array $build
   *   The entity build.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The display.
   * @param $view_mode
   *   The view mode.
   *
   * @see \rest_view_mode_entity_view().
   */
  public function entityView(
    array &$build,
    EntityInterface $entity,
    EntityViewDisplayInterface $display,
    $view_mode
  );

  /**
   * Loads a property from its extra field name.
   *
   * @param string $name
   *   The name of the extra field from which to load an entity property.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity from which to load the property.
   *
   * @return ?\Drupal\Core\Field\FieldItemListInterface
   *   The loaded extra field.
   */
  public function getProperty(string $name, EntityInterface $entity): ?FieldItemListInterface;

}
