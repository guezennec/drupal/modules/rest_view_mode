<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Abstract formatter plugin for normalized fields.
 */
abstract class NormalizedFieldFormatter extends FormatterBase {

  use SerializerAwareTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    /** @var SerializerInterface $serializer */
    $serializer = $container->get('serializer');
    $instance->setSerializer($serializer);

    return $instance;
  }

  /**
   * {@inheritdoc}
   * @throws ExceptionInterface
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {

    return $this->serializer->normalize($items, 'json');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'rest_field_name' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['rest_field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REST field name'),
      '#default_value' => $this->getSetting('rest_field_name'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    if ($rest_field_name = $this->getSetting('rest_field_name')) {
      $summary[] = $this->t('REST field name: @name', ['@name' => $rest_field_name]);
    }
    return $summary;
  }

}
