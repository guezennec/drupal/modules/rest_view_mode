<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized boolean' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_boolean_formatter",
 *   label = @Translation("Normalized boolean"),
 *   field_types = {
 *     "boolean"
 *   }
 * )
 */
class BooleanFormatter extends NormalizedFieldFormatter {
}
