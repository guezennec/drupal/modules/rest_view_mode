<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized long text' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_normalized_text",
 *   label = @Translation("Normalized long text"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   },
 * )
 */
class TextFormatter extends NormalizedFieldFormatter {
}
