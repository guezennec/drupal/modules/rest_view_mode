<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized options' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_options_formatter",
 *   label = @Translation("Normalized options"),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class OptionsKeyFormatter extends NormalizedFieldFormatter {
}
