<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;


/**
 * Plugin implementation of the 'Normalized String' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_string_formatter",
 *   label = @Translation("Normalized String"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *     "uri",
 *   },
 * )
 */
class StringFormatter extends NormalizedFieldFormatter {
}
