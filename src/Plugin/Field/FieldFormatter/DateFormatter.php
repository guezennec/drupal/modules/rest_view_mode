<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized Date' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_date_formatter",
 *   label = @Translation("Normalized Date"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DateFormatter extends NormalizedFieldFormatter {
}
