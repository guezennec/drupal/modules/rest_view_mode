<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'Normalized number' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_number_formatter",
 *   label = @Translation("Normalized number"),
 *   field_types = {
 *     "integer",
 *     "decimal",
 *     "float"
 *   }
 * )
 */
class NumberFormatter extends NormalizedFieldFormatter {
}
