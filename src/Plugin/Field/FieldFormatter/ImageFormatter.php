<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter as CoreImageFormatter;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Plugin implementation of the 'Normalized Image' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_image_formatter",
 *   label = @Translation("Normalized Image Url"),
 *   field_types = {
 *     "image"
 *   },
 * )
 */
class ImageFormatter extends CoreImageFormatter {

  use SerializerAwareTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id, $plugin_definition
  ): self {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    /** @var SerializerInterface $serializer */
    $serializer = $container->get('serializer');
    $instance->setSerializer($serializer);

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $image_style = $this->getSetting('image_style');
    /** @var ImageItem $item */
    foreach($items as $item) {
      $file = File::load($item->getValue()['target_id']);
      if (!empty($image_style)) {
        $elements[] = $this->serializer->normalize([
          'url' => ImageStyle::load($image_style)->buildUrl($file->getFileUri())
        ], 'json');
      }
      else {
        $elements[] = $this->serializer->normalize([
          'url' => $file->createFileUrl(),
        ], 'json');
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
        'rest_field_name' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    $form['rest_field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REST field name'),
      '#default_value' => $this->getSetting('rest_field_name'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();
    if ($rest_field_name = $this->getSetting('rest_field_name')) {
      $summary[] = $this->t('REST field name: @name', ['@name' => $rest_field_name]);
    }
    return $summary;
  }

}
