<?php

namespace Drupal\rest_view_mode\Plugin\Field\FieldFormatter;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "rest_view_mode_link_formatter",
 *   label = @Translation("Normalized link text and URL"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkFormatter extends NormalizedFieldFormatter {
}
