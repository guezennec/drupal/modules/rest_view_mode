<?php

namespace Drupal\rest_view_mode\Plugin\rest\resource;

use Symfony\Component\HttpFoundation\Response;

/**
 * Represents Rest View Mode records as resources.
 *
 * @RestResource (
 *   id = "rest_view_mode_resource",
 *   label = @Translation("Rest View Mode Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/rest_view_mode/{entity_type_id}/{identifier}/{view_mode}",
 *   }
 * )
 *
 */
class RestViewModeResource extends AbstractRestViewModeResource {

  /**
   * Responds to GET HTTP method.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $identifier
   *   The entity identifier.
   * @param string $view_mode
   *   The view mode.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The HTTP response.
   */
  public function get(string $entity_type_id, string $identifier, string $view_mode): Response {
    $content = $this->normalizeEntityViewDisplay($entity_type_id, $identifier, $view_mode);

    return new Response(json_encode($content ?: 'No resource found'));
  }

}
