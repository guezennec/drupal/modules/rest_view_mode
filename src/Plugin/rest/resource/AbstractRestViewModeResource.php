<?php

namespace Drupal\rest_view_mode\Plugin\rest\resource;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\rest\Plugin\ResourceBase;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Abstract REST resource to expose view modes.
 */
abstract class AbstractRestViewModeResource extends ResourceBase implements DependentPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected SerializerInterface $serializer;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
     $plugin_id,
     $plugin_definition
  ): self {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    /** @var ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    $instance->setConfigFactory($config_factory);

    /** @var EntityTypeManagerInterface $entityTypeManager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->setEntityTypeManager($entity_type_manager);

    /** @var RequestStack $requestStack */
    $request_stack = $container->get('request_stack');
    $instance->setRequestStack($request_stack);

    /** @var SerializerInterface $serializer */
    $serializer = $container->get('serializer');
    $instance->setSerializer($serializer);

    return $instance;
  }

  /**
   * Sets the config factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function setConfigFactory(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the request stack.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function setRequestStack(RequestStack $request_stack): void {
    $this->requestStack = $request_stack;
  }

  /**
   * Sets the serializer.
   *
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   */
  public function setSerializer(SerializerInterface $serializer): void {
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    return [];
  }

  /**
   * Loads an entity object from its type and identifier.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param string $id
   *   The identifier of the entity.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The loaded entity.
   */
  protected function loadEntity(string $entity_type_id, string $id): FieldableEntityInterface {
    try {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($id);
    }
    catch (Exception $exception) {
      throw new BadRequestHttpException($exception->getMessage());
    }
    if (!$entity) {
      throw new NotFoundHttpException('The entity was not found.');
    }
    if (!($entity instanceof FieldableEntityInterface)) {
      throw new NotFoundHttpException('The entity has nothing to expose.');
    }

    return $entity;
  }

  /**
   * Check if an entity type is activated and can expose a view mode.
   *
   * @param $entity_type_id
   *   The entity type id.
   * @param $view_mode
   *   The view mode.
   *
   * @return bool
   *   TRUE if entity type is activated.
   */
  protected function isActivated($entity_type_id, $view_mode): bool {
    $config = $this->configFactory->get('rest_view_mode.settings');
    $entity_config = $config->get('entities.' . $entity_type_id);

    return (
      $entity_config['activated'] &&
      (empty($entity_config['view_modes']) || in_array($view_mode, $entity_config['view_modes']))
    );
  }

  /**
   * Normalize an EntityViewDisplay config entity.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $identifier
   *   The entity identifier.
   * @param string $view_mode
   *   The view mode.
   *
   * @return array
   *   The normalized EntityViewDisplay config entity.
   */
  protected function normalizeEntityViewDisplay(string $entity_type_id, string $identifier, string $view_mode): array {
    $normalized_content = [];

    if (!$this->isActivated($entity_type_id, $view_mode)) {
      throw new NotFoundHttpException('Not found.');
    }

    if ($entity = $this->loadEntity($entity_type_id, $identifier)) {
      $entity_view_display = EntityViewDisplay::collectRenderDisplay($entity, $view_mode);
      $context = [
        'entity' => $entity,
        'view_mode' => $view_mode,
        'page' => $this->requestStack->getCurrentRequest()->get('page'),
        'limit' => $this->requestStack->getCurrentRequest()->get('limit'),
        'query' => $this->requestStack->getCurrentRequest()->query->all(),
      ];
      $normalized_content = $this->serializer->normalize($entity_view_display, 'json', $context);
    }

    return $normalized_content;
  }

}
