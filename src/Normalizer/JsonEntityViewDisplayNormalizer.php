<?php

namespace Drupal\rest_view_mode\Normalizer;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\rest_view_mode\ExtraFieldManager;
use Drupal\serialization\Normalizer\ConfigEntityNormalizer;

class JsonEntityViewDisplayNormalizer extends ConfigEntityNormalizer {

  /**
   * The extra field manager.
   *
   * @var \Drupal\rest_view_mode\ExtraFieldManager
   */
  protected ExtraFieldManager $extraFieldManager;

  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeRepositoryInterface $entity_type_repository,
    EntityFieldManagerInterface $entity_field_manager,
    ExtraFieldManager $extra_field_manager
  ) {
    parent::__construct(
      $entity_type_manager,
      $entity_type_repository,
      $entity_field_manager
    );
    $this->extraFieldManager = $extra_field_manager;
  }

  public function normalize($object, $format = NULL, array $context = []) {

    if (!isset($context['entity'])) {
      return [];
    }

    $attributes = [];
    $components = $object->toArray()['content'];
    uasort($components, [SortArray::class, 'sortByWeightElement']);
    foreach ($components as $name => $options) {
      /** @var FormatterBase $renderer */
      if ($renderer = $object->getRenderer($name)) {
        if ($context['entity']->get($name)->access('view')) {
          $rest_name = $renderer->getSetting('rest_field_name') ?: $name;
          $attributes[$rest_name] = $renderer->viewElements($context['entity']->get($name), 'fr');
        }
      }
      else if ($property = $this->extraFieldManager->getProperty($name, $context['entity'])) {
        $attributes[$name] = $this->serializer->normalize($property, 'json');
      }
      else {
        // TODO: handle core & other extra fields
        // Example for User entities: member_for
        // Example for Node entities: links
        // And any Extra Field defined by extra_field module, starting by 'extra_field_'...
      }
    }

    return $attributes;
  }

  public function supportsNormalization($data, $format = NULL) {
    return (
      parent::supportsNormalization($data, $format) &&
      $data instanceof EntityViewDisplayInterface
    );
  }

}
