# Rest View Mode

**This module leverages the power of rest resources, formatters and serializers to expose entities through view modes and form modes. In fine, it exposes EntityViewDisplay core config entities, which are a combination of an entity and a view mode.**

## Dependencies

Rest View Mode relies on core Rest module.

## Configuration

By default, nothing is enabled. You need some simple steps to configure your resources:

- Enable the "Rest View Mode Resource" and give some permissions to user roles of your choice for this resource.
- Create (or re-use) a view mode for the entity types you want to expose (like a view mode named "API" for example)
- Enable some fields inside your view mode and choose normalized formatters for display.
- Navigate to `/admin/config/services/rest-view-mode` (Configuration > Web Services > Rest View Mode settings)
- Activate the entity types of your choice, the view modes that will be exposed (like the "API" one you may have created), and maybe some properties that will be activated as extra fields inside the view mode of your entity type.
- Your resource is now enable here: `/api/rest_view_mode/{entity_type_id}/{entity_id}/{view_mode}`

## Extend

### Custom name for exposed fields

You can set any name you want inside each exposed view mode, when you choose a normalized formatter. A setting form is associated with it, allowing you to chose, for example, "customName" to replace default "field_custom_name".

### Custom name for exposed entity properties

TODO

### Custom resource path

You need to create another rest resource Plugin inside your module. You can copy `src/Plugin/rest/resource/RestViewModeResource.php` inside the same folder of your module and change the name and resource path.

### Custom formatter

This module contains normalized formatters for core fields. If you need some custom or contrib modules to have a normalized formatters for the fields they provide, you need to create it in your own module or to create an issue in the contrib module page. You can find some examples inside Formatter folder : `src/Plugin/Field/FieldFormatter`